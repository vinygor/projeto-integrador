﻿namespace RestauranteADM.TELAS.Consulta
{
    partial class alterarfuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnalterar = new System.Windows.Forms.Button();
            this.chbcadastro = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.chbcompras = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.chbfinanceiro = new System.Windows.Forms.CheckBox();
            this.chbrh = new System.Windows.Forms.CheckBox();
            this.chbvendas = new System.Windows.Forms.CheckBox();
            this.chbtotal = new System.Windows.Forms.CheckBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtsalario = new System.Windows.Forms.TextBox();
            this.txtendereço = new System.Windows.Forms.TextBox();
            this.txtrg = new System.Windows.Forms.TextBox();
            this.RG = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.salario = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnalterar
            // 
            this.btnalterar.Location = new System.Drawing.Point(427, 292);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(75, 23);
            this.btnalterar.TabIndex = 77;
            this.btnalterar.Text = "alterar";
            this.btnalterar.UseVisualStyleBackColor = true;
            this.btnalterar.Click += new System.EventHandler(this.btnCadastrarusuario_Click);
            // 
            // chbcadastro
            // 
            this.chbcadastro.AutoSize = true;
            this.chbcadastro.Location = new System.Drawing.Point(412, 154);
            this.chbcadastro.Name = "chbcadastro";
            this.chbcadastro.Size = new System.Drawing.Size(15, 14);
            this.chbcadastro.TabIndex = 76;
            this.chbcadastro.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(438, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 16);
            this.label9.TabIndex = 75;
            this.label9.Text = "Cadastro";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(438, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 16);
            this.label6.TabIndex = 74;
            this.label6.Text = "Compras";
            // 
            // chbcompras
            // 
            this.chbcompras.AutoSize = true;
            this.chbcompras.Location = new System.Drawing.Point(412, 202);
            this.chbcompras.Name = "chbcompras";
            this.chbcompras.Size = new System.Drawing.Size(15, 14);
            this.chbcompras.TabIndex = 73;
            this.chbcompras.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Yellow;
            this.label36.Location = new System.Drawing.Point(239, 38);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(192, 30);
            this.label36.TabIndex = 72;
            this.label36.Text = "Cadastro usuário ";
            // 
            // chbfinanceiro
            // 
            this.chbfinanceiro.AutoSize = true;
            this.chbfinanceiro.Location = new System.Drawing.Point(412, 226);
            this.chbfinanceiro.Name = "chbfinanceiro";
            this.chbfinanceiro.Size = new System.Drawing.Size(15, 14);
            this.chbfinanceiro.TabIndex = 71;
            this.chbfinanceiro.UseVisualStyleBackColor = true;
            // 
            // chbrh
            // 
            this.chbrh.AutoSize = true;
            this.chbrh.Location = new System.Drawing.Point(412, 251);
            this.chbrh.Name = "chbrh";
            this.chbrh.Size = new System.Drawing.Size(15, 14);
            this.chbrh.TabIndex = 70;
            this.chbrh.UseVisualStyleBackColor = true;
            // 
            // chbvendas
            // 
            this.chbvendas.AutoSize = true;
            this.chbvendas.Location = new System.Drawing.Point(412, 177);
            this.chbvendas.Name = "chbvendas";
            this.chbvendas.Size = new System.Drawing.Size(15, 14);
            this.chbvendas.TabIndex = 69;
            this.chbvendas.UseVisualStyleBackColor = true;
            // 
            // chbtotal
            // 
            this.chbtotal.AutoSize = true;
            this.chbtotal.Location = new System.Drawing.Point(412, 128);
            this.chbtotal.Name = "chbtotal";
            this.chbtotal.Size = new System.Drawing.Size(15, 14);
            this.chbtotal.TabIndex = 68;
            this.chbtotal.UseVisualStyleBackColor = true;
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(169, 389);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(134, 20);
            this.txtsenha.TabIndex = 67;
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(169, 360);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(134, 20);
            this.txtusuario.TabIndex = 66;
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(169, 154);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(183, 20);
            this.txtcpf.TabIndex = 64;
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(169, 102);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(183, 20);
            this.txtnome.TabIndex = 63;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(438, 128);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 16);
            this.label12.TabIndex = 62;
            this.label12.Text = "Total";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(438, 249);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 16);
            this.label11.TabIndex = 61;
            this.label11.Text = "RH";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(438, 177);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 16);
            this.label10.TabIndex = 60;
            this.label10.Text = "Vendas";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(438, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Financeiro ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(409, 86);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 58;
            this.label7.Text = "Permissões";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(104, 390);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 57;
            this.label5.Text = "Senha";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(104, 364);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 56;
            this.label4.Text = "Usuário";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(166, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 16);
            this.label2.TabIndex = 54;
            this.label2.Text = "CPF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(166, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 16);
            this.label1.TabIndex = 53;
            this.label1.Text = "Nome";
            // 
            // txtsalario
            // 
            this.txtsalario.Location = new System.Drawing.Point(115, 258);
            this.txtsalario.Name = "txtsalario";
            this.txtsalario.Size = new System.Drawing.Size(262, 20);
            this.txtsalario.TabIndex = 83;
            // 
            // txtendereço
            // 
            this.txtendereço.Location = new System.Drawing.Point(115, 206);
            this.txtendereço.Name = "txtendereço";
            this.txtendereço.Size = new System.Drawing.Size(262, 20);
            this.txtendereço.TabIndex = 82;
            // 
            // txtrg
            // 
            this.txtrg.Location = new System.Drawing.Point(115, 180);
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(262, 20);
            this.txtrg.TabIndex = 81;
            // 
            // RG
            // 
            this.RG.AutoSize = true;
            this.RG.BackColor = System.Drawing.Color.Silver;
            this.RG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RG.Location = new System.Drawing.Point(43, 182);
            this.RG.Name = "RG";
            this.RG.Size = new System.Drawing.Size(28, 16);
            this.RG.TabIndex = 80;
            this.RG.Text = "Rg";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(20, 206);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 16);
            this.label17.TabIndex = 79;
            this.label17.Text = "txtendereço";
            // 
            // salario
            // 
            this.salario.AutoSize = true;
            this.salario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salario.Location = new System.Drawing.Point(43, 258);
            this.salario.Name = "salario";
            this.salario.Size = new System.Drawing.Size(56, 16);
            this.salario.TabIndex = 78;
            this.salario.Text = "salario";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 84;
            this.button1.Text = "volta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // alterarfuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 573);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtsalario);
            this.Controls.Add(this.txtendereço);
            this.Controls.Add(this.txtrg);
            this.Controls.Add(this.RG);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.salario);
            this.Controls.Add(this.btnalterar);
            this.Controls.Add(this.chbcadastro);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chbcompras);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.chbfinanceiro);
            this.Controls.Add(this.chbrh);
            this.Controls.Add(this.chbvendas);
            this.Controls.Add(this.chbtotal);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.txtusuario);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "alterarfuncionario";
            this.Text = "alterarfuncionario";
            this.Load += new System.EventHandler(this.alterarfuncionario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.CheckBox chbcadastro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chbcompras;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chbfinanceiro;
        private System.Windows.Forms.CheckBox chbrh;
        private System.Windows.Forms.CheckBox chbvendas;
        private System.Windows.Forms.CheckBox chbtotal;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtsalario;
        private System.Windows.Forms.TextBox txtendereço;
        private System.Windows.Forms.TextBox txtrg;
        private System.Windows.Forms.Label RG;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label salario;
        private System.Windows.Forms.Button button1;
    }
}