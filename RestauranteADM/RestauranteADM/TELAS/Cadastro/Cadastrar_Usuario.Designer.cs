﻿namespace RestauranteADM.TELAS
{
    partial class Cadastro_usuário_do_sistema
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cadastro_usuário_do_sistema));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtcpf = new System.Windows.Forms.TextBox();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.chbtotal = new System.Windows.Forms.CheckBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.chbvendas = new System.Windows.Forms.CheckBox();
            this.chbrh = new System.Windows.Forms.CheckBox();
            this.chbfinanceiro = new System.Windows.Forms.CheckBox();
            this.label36 = new System.Windows.Forms.Label();
            this.chbcompras = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chbcadastro = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCadastrarusuario = new System.Windows.Forms.Button();
            this.txtsalario = new System.Windows.Forms.TextBox();
            this.txtendereço = new System.Windows.Forms.TextBox();
            this.txtrg = new System.Windows.Forms.TextBox();
            this.RG = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.salario = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nome";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "CPF";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(494, 320);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Usuário";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(494, 346);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "Senha";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(556, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Permissões";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(585, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(85, 16);
            this.label8.TabIndex = 8;
            this.label8.Text = "Financeiro ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(585, 114);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(61, 16);
            this.label10.TabIndex = 10;
            this.label10.Text = "Vendas";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(585, 186);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 16);
            this.label11.TabIndex = 11;
            this.label11.Text = "RH";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(585, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 16);
            this.label12.TabIndex = 12;
            this.label12.Text = "Total";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(34, 73);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(183, 20);
            this.txtnome.TabIndex = 13;
            this.txtnome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyPressNome);
            // 
            // txtcpf
            // 
            this.txtcpf.Location = new System.Drawing.Point(34, 125);
            this.txtcpf.Name = "txtcpf";
            this.txtcpf.Size = new System.Drawing.Size(183, 20);
            this.txtcpf.TabIndex = 14;
            this.txtcpf.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.txtcpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyPressCPF);
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(559, 316);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(134, 20);
            this.txtusuario.TabIndex = 16;
            this.txtusuario.TextChanged += new System.EventHandler(this.txtusuario_TextChanged);
            // 
            // txtsenha
            // 
            this.txtsenha.Location = new System.Drawing.Point(559, 345);
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(134, 20);
            this.txtsenha.TabIndex = 17;
            this.txtsenha.TextChanged += new System.EventHandler(this.txtsenha_TextChanged);
            // 
            // chbtotal
            // 
            this.chbtotal.AutoSize = true;
            this.chbtotal.Location = new System.Drawing.Point(559, 65);
            this.chbtotal.Name = "chbtotal";
            this.chbtotal.Size = new System.Drawing.Size(15, 14);
            this.chbtotal.TabIndex = 18;
            this.chbtotal.UseVisualStyleBackColor = true;
            this.chbtotal.CheckedChanged += new System.EventHandler(this.chbtotal_CheckedChanged);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // chbvendas
            // 
            this.chbvendas.AutoSize = true;
            this.chbvendas.Location = new System.Drawing.Point(559, 114);
            this.chbvendas.Name = "chbvendas";
            this.chbvendas.Size = new System.Drawing.Size(15, 14);
            this.chbvendas.TabIndex = 19;
            this.chbvendas.UseVisualStyleBackColor = true;
            this.chbvendas.CheckedChanged += new System.EventHandler(this.chbvendas_CheckedChanged);
            // 
            // chbrh
            // 
            this.chbrh.AutoSize = true;
            this.chbrh.Location = new System.Drawing.Point(559, 188);
            this.chbrh.Name = "chbrh";
            this.chbrh.Size = new System.Drawing.Size(15, 14);
            this.chbrh.TabIndex = 21;
            this.chbrh.UseVisualStyleBackColor = true;
            this.chbrh.CheckedChanged += new System.EventHandler(this.chbrh_CheckedChanged);
            // 
            // chbfinanceiro
            // 
            this.chbfinanceiro.AutoSize = true;
            this.chbfinanceiro.Location = new System.Drawing.Point(559, 163);
            this.chbfinanceiro.Name = "chbfinanceiro";
            this.chbfinanceiro.Size = new System.Drawing.Size(15, 14);
            this.chbfinanceiro.TabIndex = 22;
            this.chbfinanceiro.UseVisualStyleBackColor = true;
            this.chbfinanceiro.CheckedChanged += new System.EventHandler(this.chbfinanceiro_CheckedChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.ForeColor = System.Drawing.Color.Yellow;
            this.label36.Location = new System.Drawing.Point(104, 9);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(192, 30);
            this.label36.TabIndex = 47;
            this.label36.Text = "Cadastro usuário ";
            this.label36.Click += new System.EventHandler(this.label36_Click);
            // 
            // chbcompras
            // 
            this.chbcompras.AutoSize = true;
            this.chbcompras.Location = new System.Drawing.Point(559, 139);
            this.chbcompras.Name = "chbcompras";
            this.chbcompras.Size = new System.Drawing.Size(15, 14);
            this.chbcompras.TabIndex = 48;
            this.chbcompras.UseVisualStyleBackColor = true;
            this.chbcompras.CheckedChanged += new System.EventHandler(this.chbcompras_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(585, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 16);
            this.label6.TabIndex = 49;
            this.label6.Text = "Compras";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // chbcadastro
            // 
            this.chbcadastro.AutoSize = true;
            this.chbcadastro.Location = new System.Drawing.Point(559, 91);
            this.chbcadastro.Name = "chbcadastro";
            this.chbcadastro.Size = new System.Drawing.Size(15, 14);
            this.chbcadastro.TabIndex = 51;
            this.chbcadastro.UseVisualStyleBackColor = true;
            this.chbcadastro.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(585, 91);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 16);
            this.label9.TabIndex = 50;
            this.label9.Text = "Cadastro";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // btnCadastrarusuario
            // 
            this.btnCadastrarusuario.Location = new System.Drawing.Point(313, 377);
            this.btnCadastrarusuario.Name = "btnCadastrarusuario";
            this.btnCadastrarusuario.Size = new System.Drawing.Size(90, 31);
            this.btnCadastrarusuario.TabIndex = 52;
            this.btnCadastrarusuario.Text = "Cadastrar";
            this.btnCadastrarusuario.UseVisualStyleBackColor = true;
            this.btnCadastrarusuario.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtsalario
            // 
            this.txtsalario.Location = new System.Drawing.Point(109, 260);
            this.txtsalario.Name = "txtsalario";
            this.txtsalario.Size = new System.Drawing.Size(262, 20);
            this.txtsalario.TabIndex = 65;
            this.txtsalario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyPressSalario);
            // 
            // txtendereço
            // 
            this.txtendereço.Location = new System.Drawing.Point(109, 208);
            this.txtendereço.Name = "txtendereço";
            this.txtendereço.Size = new System.Drawing.Size(262, 20);
            this.txtendereço.TabIndex = 63;
            // 
            // txtrg
            // 
            this.txtrg.Location = new System.Drawing.Point(109, 182);
            this.txtrg.Name = "txtrg";
            this.txtrg.Size = new System.Drawing.Size(262, 20);
            this.txtrg.TabIndex = 62;
            this.txtrg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyPressRG);
            // 
            // RG
            // 
            this.RG.AutoSize = true;
            this.RG.BackColor = System.Drawing.Color.Silver;
            this.RG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RG.Location = new System.Drawing.Point(37, 184);
            this.RG.Name = "RG";
            this.RG.Size = new System.Drawing.Size(28, 16);
            this.RG.TabIndex = 57;
            this.RG.Text = "Rg";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(14, 208);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 16);
            this.label17.TabIndex = 56;
            this.label17.Text = "txtendereço";
            // 
            // salario
            // 
            this.salario.AutoSize = true;
            this.salario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.salario.Location = new System.Drawing.Point(37, 260);
            this.salario.Name = "salario";
            this.salario.Size = new System.Drawing.Size(56, 16);
            this.salario.TabIndex = 54;
            this.salario.Text = "salario";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(675, 12);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 21);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 70;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(701, 12);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(31, 21);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 69;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 71;
            this.button1.Text = "volta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Cadastro_usuário_do_sistema
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(745, 516);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtsalario);
            this.Controls.Add(this.txtendereço);
            this.Controls.Add(this.txtrg);
            this.Controls.Add(this.RG);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.salario);
            this.Controls.Add(this.btnCadastrarusuario);
            this.Controls.Add(this.chbcadastro);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chbcompras);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.chbfinanceiro);
            this.Controls.Add(this.chbrh);
            this.Controls.Add(this.chbvendas);
            this.Controls.Add(this.chbtotal);
            this.Controls.Add(this.txtsenha);
            this.Controls.Add(this.txtusuario);
            this.Controls.Add(this.txtcpf);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Cadastro_usuário_do_sistema";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro_usuário_do_sistema";
            this.TransparencyKey = System.Drawing.Color.Red;
            this.Load += new System.EventHandler(this.Cadastro_usuário_do_sistema_Load);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtcpf;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.CheckBox chbtotal;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.CheckBox chbfinanceiro;
        private System.Windows.Forms.CheckBox chbrh;
        private System.Windows.Forms.CheckBox chbvendas;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chbcadastro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox chbcompras;
        private System.Windows.Forms.Button btnCadastrarusuario;
        private System.Windows.Forms.TextBox txtsalario;
        private System.Windows.Forms.TextBox txtendereço;
        private System.Windows.Forms.TextBox txtrg;
        private System.Windows.Forms.Label RG;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label salario;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
    }
}