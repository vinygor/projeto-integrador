﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestauranteADM.BASE.Login;
using RestauranteADM.BASE.Usuario;

namespace RestauranteADM.BASE.Funcionario
{
    public class FuncionarioDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }


        public string Rg { get; set; }
        public string Endereço { get; set; }

        public double Salario { get; set; }
   

        public UsuarioDTO eui { get; set; }
    }
}
